package buu.isiriporn.mathapp.Game

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.isiriporn.mathapp.R
import buu.isiriporn.mathapp.Title.TitleViewModel
import buu.isiriporn.mathapp.databinding.FragmentAdditionBinding
import kotlin.random.Random

class AdditionFragment : Fragment() {
    private lateinit var viewModel: AdditionViewModel
    private lateinit var binding: FragmentAdditionBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentAdditionBinding>(inflater,
            R.layout.fragment_addition,container,false)
        Log.i("AdditionFragment", "Called ViewModelProvider.get")
        viewModel = ViewModelProvider(this).get(AdditionViewModel::class.java)
        timer()
        Math()
        return binding.root
    }

    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1


    fun timer () {
        binding.apply {
            object : CountDownTimer(31000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    txtTimer.setText("Time   :  " + millisUntilFinished / 1000)
                }
                override fun onFinish() {
                    txtTimer.setText("Time's OUT !")
                    view?.findNavController()?.navigate(AdditionFragmentDirections.actionAdditionFragmentToScoreFragment())
                }
            }.start()
        }
    }

    private fun Math() {
        binding.apply {
            numberOfClauses++
            val firstNumRand = Random.nextInt(0, 99)
            txtNumber1.text = firstNumRand.toString()

            val secondNumRand = Random.nextInt(0, 99)
            txtNumber2.text = secondNumRand.toString()

            txtSum.text = numberOfClauses.toString()

            val answer: Int = firstNumRand + secondNumRand

            val position = Random.nextInt(0, 3)

            if (position == 1) {
                btnFirstChoice.text = (answer + 10).toString()
                btnSecondChoice.text = (answer - 1).toString()
                btnThirdChoice.text = answer.toString()
            } else if (position == 2) {
                btnFirstChoice.text = (answer - 1).toString()
                btnSecondChoice.text = answer.toString()
                btnThirdChoice.text = (answer + 10).toString()
            } else {
                btnFirstChoice.text = answer.toString()
                btnSecondChoice.text = (answer - 1).toString()
                btnThirdChoice.text = (answer + 10).toString()
            }

            txtpointRight.text = correctScore.toString()
            txtpointWrong.text = incorrectScore.toString()


            fun addCorrect(score: Int) {
                correctScore++
                binding.txtpointRight.text = correctScore.toString()
                txtShowCorrectText.text = " PING PONG! Alright!! "
            }

            fun addIncorrect(score: Int) {
                incorrectScore++
                binding.txtpointWrong.text = incorrectScore.toString()
                txtShowCorrectText.text = " BHU BHUU! It's wrong!!"
            }

            btnFirstChoice.setOnClickListener {
                if (btnFirstChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }

            btnSecondChoice.setOnClickListener {
                if (btnSecondChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }

            btnThirdChoice.setOnClickListener {
                if (btnThirdChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }
        }
    }
}