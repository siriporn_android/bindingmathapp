package buu.isiriporn.mathapp.Game

import android.util.Log
import androidx.lifecycle.ViewModel

class AdditionViewModel : ViewModel() {
    init {
        Log.i("AdditionViewModel", "AdditionViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("AdditionViewModel", "AdditionViewModel destroyed!")
    }
}