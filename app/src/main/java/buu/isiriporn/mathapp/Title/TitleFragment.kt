package buu.isiriporn.mathapp.Title

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.isiriporn.mathapp.R
import buu.isiriporn.mathapp.Title.TitleFragmentArgs
import buu.isiriporn.mathapp.Title.TitleFragmentDirections
import buu.isiriporn.mathapp.databinding.FragmentTitleBinding
class TitleFragment : Fragment() {
    private lateinit var viewModel: TitleViewModel
    private lateinit var binding : FragmentTitleBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,
            R.layout.fragment_title,container,false)
        Log.i("TitleFragment", "Called ViewModelProvider.get")
        viewModel = ViewModelProvider(this).get(TitleViewModel::class.java)

        var correctScore = TitleFragmentArgs.fromBundle(requireArguments()).correctScore
        var incorrectScore = TitleFragmentArgs.fromBundle(requireArguments()).incorrectScore

        binding.btnAdd.setOnClickListener{view : View ->
            view.findNavController()
                .navigate(TitleFragmentDirections.actionTitleFragmentToAdditionFragment())
        }
        binding.btnMinus.setOnClickListener{view : View ->
            view.findNavController()
                .navigate(TitleFragmentDirections.actionTitleFragmentToMinusFragment())
        }
        binding.btnMultiple.setOnClickListener{view : View ->
            view.findNavController()
                .navigate(TitleFragmentDirections.actionTitleFragmentToMultipleFragment())
        }
        return binding.root
    }
}