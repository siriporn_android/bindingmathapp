package buu.isiriporn.mathapp.Score

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.isiriporn.mathapp.R
import buu.isiriporn.mathapp.Title.TitleFragmentDirections
import buu.isiriporn.mathapp.databinding.FragmentScoreBinding


class ScoreFragment : Fragment() {
    private lateinit var binding: FragmentScoreBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentScoreBinding>(inflater,
            R.layout.fragment_score,container,false)
        Back()
        return binding.root
    }

    fun showSum() {
        binding.apply {
//            val sum:String = intent.getStringExtra("sum")?:"0"
//            val txtSum = findViewById<TextView>(R.id.txtSum)
//            txtSum.text = sum
        }
    }

    fun showCorrect() {
        binding.apply {
//            val correct:String = intent.getStringExtra("correct")?:"0"
//            val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
//            txtCorrect.text = correct
        }
    }

    fun showInCorrect() {
        binding.apply {
//            val incorrect:String = intent.getStringExtra("incorrect")?:"0"
//            val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
//            txtIncorrect.text = incorrect
        }
    }

    fun Back() {
        binding.apply {
            btnBack.setOnClickListener{view : View ->
                view.findNavController()
                    .navigate(ScoreFragmentDirections.actionScoreFragmentToTitleFragment())
            }
        }
    }
}